const {server} = require("./config");
const registerMiddlewares = require("./middlewares");


async function main() {
    registerMiddlewares(server);
    server.get("/",(req, res, next) =>{
        res.json({ "message": `${req.method}` })
    });

    server.post("/",(req, res, next) =>{
        res.json({ "message": `${req.method}` })
    });

    server.put("/",(req, res, next) =>{
        res.json({ "message": `${req.method}` })
    });

    server.delete("/",(req, res, next) =>{
        res.json({ "message": `${req.method}` })
    });

    server.listen();
}

main();