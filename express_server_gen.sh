#!/bin/bash
npm init
git init
mkdir scripts/ middlewares/ config/ Controllers/ env/ utils/ models/ routes/ test/

for folder in $(ls -d */)
do
    case "$folder" in 

        *"scripts"*);;

        *"env"*);;
        *) touch $folder/index.js
            echo "adding index.js to $folder";;
    esac

done


touch server.js

touch Dockerfile Dockerfile.test

echo "node_modules" >> .gitignore && 
cp .gitignore .dockerignore &&
touch .eslintignore

npm i express cors express-basic-auth helmet eslint

./node_modules/.bin/eslint --init